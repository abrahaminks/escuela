from entidades.Maestro.models import Maestro
from django.db.models import Sum,Min,Max,Count

sueldo_total = 0.0

Maestro.objects.aggregate(sueldo_total=Sum('sueldo'))

print(f"Sueldo total de los maestros es: ${sueldo_total}")