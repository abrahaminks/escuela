from django.db import models
from Maestro.models import Maestro

# Create your models here.
class Salon(models.Model):
    codigo = models.TextField(primary_key=True, blank=False, null=False)
    letra = models.TextField(max_length= 100)
    numero = models.CharField(max_length=20, default='1')
    Maestro = models.ForeignKey(Maestro, null=True, blank=True, on_delete=models.CASCADE)