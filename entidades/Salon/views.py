from django.shortcuts import render
from django.template import loader
from Salon.models import Salon
from django.http import HttpResponse

# Create your views here.
def salones(request):
    salones = Salon.objects.all().values();
    template = loader.get_template('salones.html')
    context ={
        'salones': salones,
    }
    return HttpResponse(template.render(context, request))