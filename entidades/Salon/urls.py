from django.urls import path
from . import views

urlpatterns = [
    path('salones/', views.salones, name='salones'),
]