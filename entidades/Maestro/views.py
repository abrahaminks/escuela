from django.http import HttpResponse
from django.http import JsonResponse
from django.template import loader
from entidades.utils import Utils
from Maestro.models import Maestro
from Salon.models import Salon
from django.views.decorators.csrf import csrf_exempt
import json

@csrf_exempt
def maestros(request):
    if(request.method == 'POST'):
        data = json.loads(request.body)
        maestro = Maestro(nombre_completo=data['nombre_completo'],sueldo=data['sueldo'])
        maestro.save()
        for salon in data['salones']:
            s = Salon(codigo=salon['Id'], letra=salon['Letra'], numero=salon['Numero'], Maestro=maestro)
            s.save()
            
        return HttpResponse(data)
    else:
        maestros = Maestro.objects.all().values()
        template = loader.get_template('maestros_template.html')
        context ={
            'maestros': maestros,
        }
        return HttpResponse(template.render(context, request))
    

def details(request,id):
    maestro = Maestro.objects.get(id=id)
    template = loader.get_template('details.html')
    context = {
        'maestro': maestro,
    }
    return HttpResponse(template.render(context, request))

def delete(request,id):
    maestro = Maestro.objects.get(id=id)
    maestro.delete()
    maestros = Maestro.objects.all().values()
    template = loader.get_template('maestros_template.html')
    context ={
        'maestros': maestros,
    }
    return HttpResponse(template.render(context, request))