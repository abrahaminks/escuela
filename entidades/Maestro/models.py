from django.db import models

# Create your models here.
class Maestro(models.Model):
    id = models.IntegerField(primary_key=True, blank=False, null=False)
    nombre_completo = models.TextField(max_length= 100)
    sueldo = models.FloatField(null=False, blank=False, default=1000)