from django.urls import path
from . import views

urlpatterns = [
    path('maestros/', views.maestros, name='maestros'),
    path('maestros/details/<int:id>', views.details, name='details'),
    path('maestros/delete/<int:id>', views.delete, name='delete'),
]